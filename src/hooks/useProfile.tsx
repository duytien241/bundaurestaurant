import { getProfile } from 'api/profile';
import { useQuery } from 'react-query';

export default function useProfile() {
  const { data: profile, refetch: refetchProfile } = useQuery<any>('profile', getProfile, { enabled: true });
  return { profile, refetchProfile };
}

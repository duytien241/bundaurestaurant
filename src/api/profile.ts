import { sendGet, sendPut } from './axios';

// eslint-disable-next-line import/prefer-default-export
export const getProfile = () => sendGet('/v1/me');
export const updateProfile = () => sendPut('/v1/app/profile');

import { sendGet, sendPost, sendPut } from './axios';

// eslint-disable-next-line import/prefer-default-export
export const getListMenu = (params: any) => sendGet('/v1/menu', params);
export const addToCart = (params: any) => sendPost('/v1/cart/', params);
export const getListCart = () => sendGet('/v1/cart');
export const getListOrder = () => sendGet('/v1/orders');

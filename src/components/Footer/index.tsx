import React from 'react';
import styles from './styles.module.scss';
import GoogleMapReact from 'google-map-react';
import Marker from 'components/Marker';

export default function Footer() {
  const defaultProps = {
    center: {
      lat: 21.0074254,
      lng: 105.8434204,
    },
    zoom: 15,
  };
  return (
    <div className={styles.footerWrapper}>
      <GoogleMapReact
        bootstrapURLKeys={{ key: 'AIzaSyCA7VP7ey46smh1sFjQXVB9EoM29w5Wct0' }}
        defaultCenter={defaultProps.center}
        defaultZoom={defaultProps.zoom}
      >
        <Marker lat={21.0074254} lng={105.8434204} text={'Bún đậu Bách khoa'} />
      </GoogleMapReact>
    </div>
  );
}

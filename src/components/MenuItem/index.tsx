import { Col, Input, Row } from 'antd';
import { addToCart } from 'api/menu';
import CustomNotification from 'components/CustomNotification';
import { handleErrorMessage } from 'helper';
import { formatNumber } from 'helper/common';
import { Obj } from 'interfaces';
import React, { useContext, useState } from 'react';
import { useMutation } from 'react-query';
import styles from './styles.module.scss';

interface IProps {
  data: Obj;
}
export default function MenuItem({ data }: IProps) {
  const [qual, setQual] = useState(1);
  const { mutate, isLoading: isLoadingUpdate } = useMutation(addToCart, {
    onError: (error) => handleErrorMessage(error),
    onSuccess: (data) => {
      CustomNotification({
        type: 'success',
        message: 'Thành công',
        description: 'Thêm sản phẩm thành công',
      });
    },
  });

  const handleAddCart = () => {
    mutate({
      quantity: qual,
      user: 1,
      item: data?.id,
      total_price: data?.price,
    });
  };
  return (
    <div className={styles.menuItem}>
      <Col>
        <div className={styles.header}>
          <img className={styles.imgItem} src={data?.src_image as string} alt="" />
          <div className={styles.price}>
            <div>Giá</div>
            <div>{formatNumber(Number(data?.price))}</div>
            <div>VND</div>
          </div>
        </div>
        <div className={styles.content}>
          <div className={styles.name}> {data?.name}</div>
          <div className={styles.divider}>*****</div>
          <div className={styles.description}>{data?.description}</div>
        </div>
        <Row className={styles.btnContainer}>
          <Col>
            <div className={styles.btn} onClick={() => handleAddCart()}>
              Đặt món ngay
            </div>
          </Col>
          <Col>
            <Input
              type="number"
              className={styles.input}
              min={1}
              defaultValue={1}
              step={1}
              onChange={(value) => setQual(Number(value.target.value))}
            />
          </Col>
        </Row>
      </Col>
    </div>
  );
}

import React from 'react';
import markerMap from 'assets/images/markerMap.svg';

import styles from './styles.module.scss';

interface IMarkerSite {
  lat: number;
  lng: number;
  text: string;
}
export default function Marker({ text, lat, lng }: IMarkerSite) {
  return (
    <div className={styles.markerContainer}>
      <img src={markerMap} alt="" />
      {/* <div className={styles.siteName}>{text}</div> */}
    </div>
  );
}

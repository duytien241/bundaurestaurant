import React from 'react';
import _ from 'lodash';
import styles from './style.module.scss';
import { Card, Col, Row } from 'antd';
import classNames from 'classnames';
import bundau1 from 'assets/images/bundau1.jpg';
import bundau2 from 'assets/images/bundau2.jpg';
import { Link } from 'react-router-dom';
import MenuItem from 'components/MenuItem';

export default function Home() {
  return (
    <div className={styles.homeContainer}>
      <Row justify="space-around">
        <Col xs={18}>
          <div className={styles.titleContainer}>
            <div className={styles.title}>BÚN ĐẬU</div>
            <div className={styles.description}>YOUR VIETNAMESE CUISINE JOURNEY STARTS HERE</div>
          </div>
        </Col>
        <Col xs={6} className={styles.colRightParrent}>
          <div className={styles.addressContainer}>
            <div className={styles.title}>Địa chỉ</div>
            <div className={classNames(styles.info, styles.name)}>Bún Đậu Bách Khoa</div>
            <div className={styles.divider}>******</div>
            <div className={classNames(styles.info)}>Số 1 Đại Cồ Việt, Hai Bà Trưng, Hà Nội</div>
            <div className={styles.divider}>******</div>
            <div className={classNames(styles.info)}>0868686868</div>
            <div className={styles.divider}>******</div>
            <div className={classNames(styles.info)}>bundau.bk@hust.edu.vn</div>
          </div>
        </Col>
      </Row>

      <Row className={styles.buttonOrder}>
        <Link to="/menu" className={styles.btnLink}>
          Đặt món ngay
        </Link>
      </Row>
      <Row justify="space-around">
        <Col xs={18}>
          <div className={styles.infoContainer}>
            <div className={styles.title}>ĐẶC SẢN ĐẤT HÀ THÀNH</div>
            <Row className={styles.content}>
              <Col xs={14} className={styles.colleft}>
                <div className={styles.description}>
                  Hà Nội là cái nôi của một nền ẩm thực giản dị, đơn sơ nhưng chứa đựng trong từng món ăn sự chăm chút,
                  tinh tế và tỉ mỉ của người nấu. Chỉ cần một chút thay đổi trong cách chế biến thì người Hà Nội cũng
                  nhận ra món ăn đã bị biến thể ít nhiều. Đến đậu hũ cho món bún đậu cũng phải chiên lửa thật khéo, để
                  mặt ngoài vẫn giòn nhưng bên trong thì đậu vẫn còn mềm, béo và thơm khi cắt ra, hay đến món chả rươi
                  cũng phải làm thật kỹ lưỡng để chả không bị khô, và rươi trong chả vẫn không bị mất vị. Đến chén nước
                  chấm cũng phải tỉ mỉ trong từng nguyên liệu thêm vào, vì chỉ thiếu một thành phần thì phần linh hồn
                  của món ăn đã mất đi quá nửa.
                </div>
                <img className={styles.imgSmall} src={bundau1} alt="" />
              </Col>
              <Col xs={10} className={styles.colRight}>
                <img className={styles.imgSmall2} src={bundau2} alt="" />
              </Col>
            </Row>
          </div>
        </Col>

        <Col xs={6} className={styles.colRightParrent}>
          <div className={styles.descriptionHN}>
            <div className={styles.title}> MỘT VÒNG HÀ THÀNH</div>
            <div className={styles.content}>
              Hà Nội là thủ đô của nước Cộng hòa xã hội chủ nghĩa Việt Nam và cũng là kinh đô của hầu hết các vương
              triều phong kiến Việt trước đây. Do đó, lịch sử Hà Nội gắn liền với sự thăng trầm của lịch sử Việt Nam qua
              các thời kỳ.
            </div>
            <div className={styles.content}>
              Hà Nội là thành phố trực thuộc trung ương có diện tích lớn nhất cả nước từ khi tỉnh Hà Tây sáp nhập vào,
              đồng thời cũng là địa phương đứng thứ nhì về dân số với gần 8 triệu người (năm 2018), sau Thành phố Hồ Chí
              Minh. Tuy nhiên, nếu tính những người cư trú không đăng ký thì dân số thực tế của thành phố này năm 2017
              là hơn 9 triệu người.
            </div>
          </div>
        </Col>
      </Row>
      <Row justify="center">
        <Col xs={24}>
          <div className={styles.infoContainer2}>
            <div className={styles.title}>BÍ QUYẾT MẮM TÔM</div>
            <Row className={styles.content}>
              <div className={styles.description}>
                Để có một bát mắm tôm ngon, chuẩn vị Hà Thành, cần phải pha đúng kiểu:
                <ul>
                  <li>
                    <span>Bước 1: Mắm tôm loại I được cho vào một bát con, vắt thêm 1 – 2 quả quất.</span>
                  </li>
                  <li>
                    <span>
                      Bước 2: Cho thêm 1 ít đường nâu để mắm có vị ngọt dịu, không hăng, không gắt. Đổ thêm ít dầu chiên
                      nóng còn đang xèo xèo, vừa giúp đánh bông mắm tôm dễ dàng, lại làm mắm tôm vừa thơm vừa chín.
                    </span>
                  </li>
                  <li>
                    <span>Bước 3: Dùng đũa nhanh tay đánh hỗn hợp đến khi lớp mắm tôm bông lên.</span>
                  </li>
                  <li>
                    <span>Bước 4: Ớt tươi thái lát, cho thêm vào bát mắm tôm, tạo vị cay nhẹ “hít hà” ngon ngon.</span>
                  </li>
                </ul>
                Hãy pha một bát mắm tôm theo tuần tự các bước nhà Đậu hướng dẫn, đừng đổ lỗi cho Đậu nếu quý khách lỡ
                “nghiện” món này nhé!
              </div>
            </Row>
          </div>
        </Col>
      </Row>
    </div>
  );
}

import React, { useEffect, useState } from 'react';
import _ from 'lodash';
import styles from './style.module.scss';
import { Button, Card, Col, Form, Input, Row } from 'antd';
import classNames from 'classnames';
import { formatNumber } from 'helper/common';
import { getListCart } from 'api/menu';
import { useQuery } from 'react-query';
import axios from 'axios';
import Cookies from 'js-cookie';
import { handleErrorMessage } from 'helper';
import CustomNotification from 'components/CustomNotification';
import useProfile from 'hooks/useProfile';
import useDebounce from 'hooks/useDebounce';
import { history } from 'App';

export default function Home() {
  const [dataCart, setData] = useState<any[]>();
  const [isInfo, setIsInfo] = useState(false);
  const [tax, setTax] = useState(20000);
  const { profile } = useProfile();
  const [form] = Form.useForm();
  const [address, setAddress] = useState('');
  const debouncedSearchTerm = useDebounce(address, 700);
  const { data: listMenu } = useQuery(['listCart'], () => getListCart(), {
    keepPreviousData: true,
  });

  useEffect(() => {
    setData(listMenu);
  }, [listMenu]);

  useEffect(() => {
    const text = String(debouncedSearchTerm)
      .replace(/[\u{0080}-\u{FFFF}]/gu, '')
      .toLocaleLowerCase();
    if (text.includes('thanh tri') || text.includes('ha dong')) {
      setTax(30000);
    } else if (text.includes('nam tu niem') || text.includes('trung van')) {
      setTax(2700);
    } else if (text.includes('cau giay')) {
      setTax(2800);
    } else if (text.includes('my dinh')) {
      setTax(2900);
    } else if (text.includes('thanh oai') || text.includes('that that')) {
      setTax(40000);
    } else if (text.includes('dong da') || text.includes('hai ba trung')) {
      setTax(25000);
    } else {
      setTax(25000);
    }
  }, [debouncedSearchTerm]);

  const changeNumber = (value: number, data: any) => {
    if (!dataCart) return;
    const index = dataCart?.findIndex((el: any) => el?.id === data?.id);
    dataCart[index].quantity = value;
    setData(() => {
      return [...dataCart];
    });
  };

  const handleChangeAddress = (value: string) => {
    setAddress(value);
  };

  const handleGetTotalPrice = (inclueShip?: boolean, isApi?: boolean) => {
    if (!dataCart) return 0;
    let res = 0;
    dataCart?.forEach((a: any) => (res += Number(a?.total_price) * Number(a.quantity)));
    if (isApi) return Number(res) + tax;
    return formatNumber((inclueShip ? tax : 0) + res);
  };

  const handleGoToConfirm = () => {
    setIsInfo(true);
  };

  const handleGoToMenu = () => {
    history.push('/menu');
  };

  const handleSubmit = (values: any) => {
    axios
      .post(
        `http://127.0.0.1:8080/v1/orders/`,
        {
          user: profile?.id,
          ship_address: values.address,
          phone: values.phone,
          totalDue: handleGetTotalPrice(true, true),
          status: 'Chưa giao hàng',
        },
        { headers: { Authorization: `Token  ${Cookies.get('token')}` } }
      )
      .then((res) => {
        dataCart?.forEach((element: any) => {
          axios
            .post(
              `http://127.0.0.1:8080/v1/order/`,
              {
                order: res.data.id,
                item: element.id,
                quantity: element.quantity,
              },
              { headers: { Authorization: `Token  ${Cookies.get('token')}` } }
            )
            .then((res) => {
              dataCart?.forEach((element: any) => {
                axios
                  .delete(`http://127.0.0.1:8080/v1/cart/` + element.id, {
                    headers: { Authorization: `Token  ${Cookies.get('token')}` },
                  })
                  .then((res) => {})
                  .catch((error) => console.log(error));
              });
              CustomNotification({
                type: 'success',
                message: 'Thành công',
                description: 'Đặt sản phẩm thành công',
              });
              window.location.href = '/order-history';
            })
            .catch((error) => console.log(error));
        });
      })
      .catch((error) => handleErrorMessage(error));
  };

  return (
    <div className={styles.homeContainer}>
      <Row justify="space-around">
        <Col xs={18}>
          <div className={styles.titleContainer}>
            <div className={styles.title}>BÚN ĐẬU</div>
            <div className={styles.description}>YOUR VIETNAMESE CUISINE JOURNEY STARTS HERE</div>
          </div>
        </Col>
        <Col xs={6} className={styles.colRightParrent}>
          <div className={styles.addressContainer}>
            <div className={styles.title}>Địa chỉ</div>
            <div className={classNames(styles.info, styles.name)}>Bún Đậu Bách Khoa</div>
            <div className={styles.divider}>******</div>
            <div className={classNames(styles.info)}>Số 1 Đại Cồ Việt, Hai Bà Trưng, Hà Nội</div>
            <div className={styles.divider}>******</div>
            <div className={classNames(styles.info)}>0868686868</div>
            <div className={styles.divider}>******</div>
            <div className={classNames(styles.info)}>bundau.bk@hust.edu.vn</div>
          </div>
        </Col>
      </Row>

      <Row justify="start">
        {isInfo ? (
          <Col xs={24}>
            <Form onFinish={handleSubmit} layout="vertical" className={styles.form} form={form}>
              <Form.Item name="address" label="Địa chỉ">
                <Input className={styles.inputForm} onChange={(e) => handleChangeAddress(e.target.value)} />
              </Form.Item>
              <Form.Item name="phone" label="Số điện thoại">
                <Input className={styles.inputForm} />
              </Form.Item>
              <Row>
                <Col xs={6}>
                  <div className={styles.total}>Tổng đơn hàng</div>
                </Col>
                <Col xs={6}>
                  <div className={styles.price}>
                    {handleGetTotalPrice()}
                    <span className={styles.unit}>đ</span>
                  </div>
                </Col>
              </Row>
              <Row>
                <Col xs={6}>
                  <div className={styles.total}>Phí ship:</div>
                </Col>
                <Col xs={6}>
                  <div className={styles.price}>
                    {formatNumber(tax)}
                    <span className={styles.unit}>đ</span>
                  </div>
                </Col>
              </Row>
              <Row>
                <Col xs={6}>
                  <div className={styles.total}>Tổng</div>
                </Col>
                <Col xs={6}>
                  <div className={styles.price}>
                    {handleGetTotalPrice(true)}
                    <span className={styles.unit}>đ</span>
                  </div>
                </Col>
              </Row>
              <div className={styles.btnContainer2}>
                <div className={styles.btnOrder} onClick={() => form.submit()}>
                  Tiếp tục
                </div>
              </div>
            </Form>
          </Col>
        ) : (
          <Col xs={24}>
            <div className={styles.infoContainer}>
              <div className={styles.title}>Giỏ hàng</div>
              <Col className={styles.content}>
                <div className={styles.header}>
                  <div className={styles.name}>Sản phẩm</div>
                  <div className={styles.price}>Giá</div>
                  <div className={styles.quantity}>Số lượng</div>
                  <div className={styles.totalPrice}>Tổng</div>
                </div>
                <div className={styles.detail}>
                  {dataCart?.map((el: any) => {
                    return (
                      <div className={styles.row}>
                        <div className={styles.name}>{el?.name}</div>
                        <div className={styles.price}> {formatNumber(Number(el?.total_price))}</div>
                        <div className={styles.quantity}>
                          <Input
                            type="number"
                            className={styles.input}
                            min={1}
                            defaultValue={el?.quantity}
                            step={1}
                            onChange={(value) => changeNumber(Number(value.target.value), el)}
                          />
                        </div>
                        <div className={styles.totalPrice}>
                          {formatNumber(Number(el?.quantity) * Number(el?.total_price))}
                        </div>
                      </div>
                    );
                  })}
                </div>

                <div className={styles.summary}>
                  <Col>
                    <div className={styles.total}>Tổng đơn hàng</div>
                    <div className={styles.price}>
                      {handleGetTotalPrice()}
                      <span className={styles.unit}>đ</span>
                    </div>
                  </Col>
                </div>
                <div className={styles.btnContainer}>
                  <div className={styles.btnOrder} onClick={() => handleGoToConfirm()}>
                    Tiến hành thanh toán
                  </div>
                </div>
                <div className={styles.btnContainer2}>
                  <div className={styles.btnOrder2} onClick={() => handleGoToMenu()}>
                    Tiếp tục mua
                  </div>
                </div>
              </Col>
            </div>
          </Col>
        )}
      </Row>
    </div>
  );
}

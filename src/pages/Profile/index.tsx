import React, { useEffect } from 'react';
import _ from 'lodash';
import styles from './style.module.scss';
import { Button, Col, Form, Input, Row } from 'antd';
import classNames from 'classnames';
import axios from 'axios';
import Cookies from 'js-cookie';
import { handleErrorMessage } from 'helper';
import CustomNotification from 'components/CustomNotification';
import useProfile from 'hooks/useProfile';

export default function Profile() {
  const [form] = Form.useForm();
  const { profile, refetchProfile } = useProfile();
  useEffect(() => {
    if (profile) {
      form.setFieldsValue({
        ...profile,
      });
    }
  }, [profile]);

  const handleSubmit = (values: any) => {
    axios
      .put(
        `http://127.0.0.1:8080/v1/user/${profile.id}/`,
        {
          phone: values.phone,
          address: values.address,
          first_name: values.first_name,
          last_name: values.last_name,
        },
        { headers: { Authorization: `Token  ${Cookies.get('token')}` } }
      )
      .then((res) => {
        refetchProfile();
        CustomNotification({
          type: 'success',
          message: 'Thành công',
          description: 'Cập nhật hồ sơ công',
        });
      })
      .catch((error) => handleErrorMessage(error));
  };

  return (
    <div className={styles.homeContainer}>
      <Row justify="space-around">
        <Col xs={18}>
          <div className={styles.titleContainer}>
            <div className={styles.title}>BÚN ĐẬU</div>
            <div className={styles.description}>YOUR VIETNAMESE CUISINE JOURNEY STARTS HERE</div>
          </div>
        </Col>
        <Col xs={6} className={styles.colRightParrent}>
          <div className={styles.addressContainer}>
            <div className={styles.title}>Địa chỉ</div>
            <div className={classNames(styles.info, styles.name)}>Bún Đậu Bách Khoa</div>
            <div className={styles.divider}>******</div>
            <div className={classNames(styles.info)}>Số 1 Đại Cồ Việt, Hai Bà Trưng, Hà Nội</div>
            <div className={styles.divider}>******</div>
            <div className={classNames(styles.info)}>0868686868</div>
            <div className={styles.divider}>******</div>
            <div className={classNames(styles.info)}>bundau.bk@hust.edu.vn</div>
          </div>
        </Col>
      </Row>

      <Row justify="center">
        <Col xs={12}>
          <Form onFinish={handleSubmit} form={form} style={{ marginTop: 50 }} layout="vertical">
            <Form.Item label="E-mail" name="email">
              <Input disabled />
            </Form.Item>
            <Form.Item label="Username" name="username">
              <Input />
            </Form.Item>
            <Form.Item label="Họ" name="first_name">
              <Input />
            </Form.Item>
            <Form.Item label="Tên" name="last_name">
              <Input />
            </Form.Item>
            <Form.Item
              label="Số điện thoại"
              name="phone"
              rules={[
                {
                  required: false,
                  message: 'Vui lòng nhập số điện thoại hợp lệ',
                },
              ]}
            >
              <Input style={{ width: '100%' }} />
            </Form.Item>
            <Form.Item label="Địa chỉ" name="addres">
              <Input />
            </Form.Item>
            <Form.Item>
              <Button type="primary" htmlType="submit" block>
                Chỉnh sửa
              </Button>
            </Form.Item>
          </Form>
        </Col>
      </Row>
    </div>
  );
}

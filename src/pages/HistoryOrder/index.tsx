import React, { useEffect, useState } from 'react';
import _ from 'lodash';
import styles from './style.module.scss';
import { Button, Card, Col, Form, Input, Row } from 'antd';
import classNames from 'classnames';
import { formatNumber } from 'helper/common';
import { getListOrder } from 'api/menu';
import { useQuery } from 'react-query';
import moment from 'moment';

export default function Home() {
  const [dataCart, setData] = useState<any[]>();
  const { data: listMenu } = useQuery(['listOrder'], () => getListOrder(), {
    keepPreviousData: true,
  });

  useEffect(() => {
    setData(listMenu);
  }, [listMenu]);

  console.log(dataCart);

  return (
    <div className={styles.homeContainer}>
      <Row justify="space-around">
        <Col xs={18}>
          <div className={styles.titleContainer}>
            <div className={styles.title}>BÚN ĐẬU</div>
            <div className={styles.description}>YOUR VIETNAMESE CUISINE JOURNEY STARTS HERE</div>
          </div>
        </Col>
        <Col xs={6} className={styles.colRightParrent}>
          <div className={styles.addressContainer}>
            <div className={styles.title}>Địa chỉ</div>
            <div className={classNames(styles.info, styles.name)}>Bún Đậu Bách Khoa</div>
            <div className={styles.divider}>******</div>
            <div className={classNames(styles.info)}>Số 1 Đại Cồ Việt, Hai Bà Trưng, Hà Nội</div>
            <div className={styles.divider}>******</div>
            <div className={classNames(styles.info)}>0868686868</div>
            <div className={styles.divider}>******</div>
            <div className={classNames(styles.info)}>bundau.bk@hust.edu.vn</div>
          </div>
        </Col>
      </Row>

      <Row className={classNames(styles.row, styles.headerRow)}>
        <div className={styles.name}>STT</div>
        <div className={styles.price}>Trạng thái</div>
        <div className={styles.price}>Địa chỉ</div>
        <div className={styles.totalPrice}>Tổng</div>
        <div className={styles.quantity}>Ngày tạo</div>
      </Row>
      {dataCart?.map((el: any) => {
        return (
          <div className={styles.row}>
            <div className={styles.name}>{el?.id}</div>
            <div className={styles.price}> {el?.status}</div>
            <div className={styles.price}> {el?.ship_address}</div>
            <div className={styles.totalPrice}>{formatNumber(Number(el?.totalDue))}</div>
            <div className={styles.quantity}>{moment(el?.created_at).format('HH:mm DD/MM/YYYY')}</div>
          </div>
        );
      })}
    </div>
  );
}

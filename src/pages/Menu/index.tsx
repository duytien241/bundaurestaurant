import React, { useState } from 'react';
import _ from 'lodash';
import styles from './style.module.scss';
import { Card, Col, Input, Row, Select } from 'antd';
import classNames from 'classnames';
import MenuItem from 'components/MenuItem';
import { useQuery } from 'react-query';
import { getListMenu } from 'api/menu';
const { Option } = Select;
export default function Menu() {
  const [filter, setFilter] = useState<any>({ pageIndex: 1, take: 10, search: null, type: null });
  const [type, setType] = useState(1);
  const { data: listMenu } = useQuery(['listMenu', filter], () => getListMenu(filter), {
    keepPreviousData: true,
  });
  const handleChangeInputSearch = (value: string) => {
    setFilter({ ...filter, search: value });
  };

  const handleChangeType = (value: number) => {
    setType(value);
    setFilter({ ...filter, type: String(value) });
  };

  return (
    <div className={styles.homeContainer}>
      <Row justify="start">
        <Col xs={18}>
          <div className={styles.titleContainer}>
            <div className={styles.title}>BÚN ĐẬU</div>
            <div className={styles.description}>YOUR VIETNAMESE CUISINE JOURNEY STARTS HERE</div>
          </div>
        </Col>
      </Row>

      <div className={styles.content}>
        <Row justify="end" className={styles.filter}>
          <Input.Search onSearch={(value) => handleChangeInputSearch(value)} className={styles.search} />

          <div className={classNames(styles.btn, { [styles.active]: type === 1 })} onClick={() => handleChangeType(1)}>
            Món chính
          </div>
          <div className={classNames(styles.btn, { [styles.active]: type === 3 })} onClick={() => handleChangeType(3)}>
            Đồ uống
          </div>
          <div className={classNames(styles.btn, { [styles.active]: type === 2 })} onClick={() => handleChangeType(2)}>
            Ăn vặt
          </div>
        </Row>
        <Row justify="space-around">
          {listMenu?.map((el: any) => {
            return (
              <Col>
                <MenuItem data={el} />
              </Col>
            );
          })}
        </Row>
      </div>
    </div>
  );
}

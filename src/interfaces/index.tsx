export interface Obj {
  // eslint-disable-next-line
  [key: string]: {} | undefined;
}

export interface NoticeProps {
  type: 'success' | 'info' | 'warning' | 'error';
  message: string;
  description: string;
}

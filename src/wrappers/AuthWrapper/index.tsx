import React, { lazy, Suspense } from 'react';
import Cookies from 'js-cookie';
import PageHeader from 'components/PageHeader';
import { Redirect, Route, Switch } from 'react-router-dom';
import styles from './styles.module.scss';
import Footer from 'components/Footer';

const Tasks = lazy(() => import('pages/Tasks'));
const Home = lazy(() => import('pages/Home'));
const Menu = lazy(() => import('pages/Menu'));
const Cart = lazy(() => import('pages/Cart'));
const Profile = lazy(() => import('pages/Profile'));
const HistoryOrder = lazy(() => import('pages/HistoryOrder'));

export default function PageWrapper() {
  const isAuthenticated = !!Cookies.get('token');
  // const { profile } = useProfile(isAuthenticated);

  if (!isAuthenticated) return <Redirect to="/login" />;
  // if (!profile) return null;
  return (
    <div className={styles.pageWrapper}>
      {/* <SideNav /> */}
      <div className={styles.mainWrapper}>
        <PageHeader />
        <div className={styles.pageContent}>
          <Suspense fallback={null}>
            <Switch>
              <Route path="/tasks" component={Tasks} />
              <Route path="/home" component={Home} exact={true} />
              <Route path="/menu" component={Menu} exact={true} />
              <Route path="/profile" component={Profile} exact={true} />
              <Route path="/cart" component={Cart} exact={true} />
              <Route path="/order-history" component={HistoryOrder} exact={true} />
            </Switch>
          </Suspense>
        </div>
        <Footer />
      </div>
    </div>
  );
}
